package in.flashbulb.myapplication;

public class MainContract {
    interface MainPresenter{
        void attach(MainView mainView, LocalQuotesInteractor localQuotesInteractor);
        void detach();
        void onGetQuotesClicked();
    }
    interface MainView{
        void showProgress();
        void hideProgress();
        void setQuote(String quote);
    }
    interface LocalQuotesInteractor {
        interface Listener{
            void OnQuoteReceived(String quote);
        }
        void getNextQuote(Listener listener);
    }
}
