package in.flashbulb.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements MainContract.MainView{

    private MainContract.MainPresenter mainPresenter;
    private TextView txtQuotes;
    private Button btnGetQuotes;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtQuotes = findViewById(R.id.txtQuote);
        btnGetQuotes = findViewById(R.id.btnGetQuote);
        progressBar = findViewById(R.id.progressBar);

        mainPresenter = new MainPresenterImpl();
        mainPresenter.attach(this, new LocalQuotesInteractorImpl());

        btnGetQuotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainPresenter.onGetQuotesClicked();
            }
        });
    }

    @Override
    public void showProgress() {
        txtQuotes.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        txtQuotes.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setQuote(String quote) {
        txtQuotes.setText(quote);
    }
}
