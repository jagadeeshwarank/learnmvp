package in.flashbulb.myapplication;

public class MainPresenterImpl implements MainContract.MainPresenter {
    MainContract.MainView mainView;
    MainContract.LocalQuotesInteractor localQuotesInteractor;

    @Override
    public void attach(MainContract.MainView mainView, MainContract.LocalQuotesInteractor localQuotesInteractor) {
        this.mainView = mainView;
        this.localQuotesInteractor = localQuotesInteractor;
    }

    @Override
    public void detach() {
        mainView = null;
    }

    @Override
    public void onGetQuotesClicked() {
        mainView.showProgress();
        localQuotesInteractor.getNextQuote(new MainContract.LocalQuotesInteractor.Listener() {
            @Override
            public void OnQuoteReceived(String quote) {
                mainView.setQuote(quote);
                mainView.hideProgress();
            }
        });
    }
}
