package in.flashbulb.myapplication;

import android.os.Handler;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class LocalQuotesInteractorImpl implements MainContract.LocalQuotesInteractor {
    List<String> motivationalQuotes = Arrays.asList(
            "Be the change you wish to see in the world",
            "If you spend too much time thinking about a thing, you’ll never get it done.",
            "Knowing is not enough, we must apply. Willing is not enough, we must do.",
            "Do not pray for an easy life, pray for the strength to endure a difficult one.",
            "Be happy, but never satisfied.");
    @Override
    public void getNextQuote(final Listener listener) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                listener.OnQuoteReceived(getRandomString());
            }
        },1200);
    }

    private String getRandomString(){
        Random random = new Random();
        int index = random.nextInt(motivationalQuotes.size());
        return motivationalQuotes.get(index);
    }
}
